from setuptools import setup, find_packages

setup(name="phyjax",
      version="0.0.0",
      install_requires=["jax"],
      extras_require={"dev": ["jaxlib", "coverage", "twine","unittest-xml-reporting", "wheel"]},
      packages=find_packages())
