import unittest
import numpy as np

import jax
import jax.numpy as jnp

from phyjax import util, core

class TestUtil(unittest.TestCase):
    def test_inv_quat(self):
        a = jnp.asarray([[1., 2., 3., 4.]])
        np.testing.assert_allclose(util.inv_quat(a), np.asarray([[-1., -2., -3., 4.]]))

        b = jnp.asarray([[1., 2., 3., 4.], [4., 5., 6., 7.]])
        np.testing.assert_allclose(util.inv_quat(b),
                                   np.asarray([[-1., -2., -3., 4.], [-4., -5., -6., 7.]]))

    def test_rvbq(self):
        ath = jnp.pi / 2
        av = jnp.asarray([[1., 0., 0.]])
        aq = jnp.asarray([[0., 0., jnp.sin(ath/2), jnp.cos(ath/2)]])
        np.testing.assert_allclose(util.rotate_vec_by_quat(av, aq),
                                   np.asarray([[0., 1., 0.]]),
                                   atol=1e-7)

        bth = jnp.pi / 2
        bv = jnp.asarray([[1., 1., 1.]])
        bq = jnp.asarray([[0., 0., jnp.sin(bth/2), jnp.cos(bth/2)]])
        np.testing.assert_allclose(util.rotate_vec_by_quat(bv, bq),
                                   np.asarray([[-1., 1., 1.]]),
                                   atol=1e-7)

    def test_r2q(self):
        ar = jnp.asarray([[1., 0., 0.]])
        np.testing.assert_allclose(util.rot2quat(ar),
                                   np.asarray([[np.sin(0.5), 0., 0., np.cos(0.5)]]))

        br = jnp.asarray([[1., 1., 0.]])
        bth = np.sqrt(2.0)
        np.testing.assert_allclose(util.rot2quat(br),
                                   np.asarray([[np.sin(bth/2)/bth, np.sin(bth/2)/bth, 0., np.cos(bth/2)]]))

    def test_qm(self):
        aq = jnp.asarray([[jnp.sin(jnp.pi/4), 0., 0., jnp.cos(jnp.pi/4)]])
        bq = jnp.asarray([[jnp.sin(jnp.pi/4), 0., 0., jnp.cos(jnp.pi/4)]])
        np.testing.assert_allclose(util.quat_mul(aq, bq),
                                   np.asarray([[np.sin(np.pi/2), 0., 0., np.cos(np.pi/2)]]),
                                   atol=1e-6)

class TestCore(unittest.TestCase):
    def test_rotate_inertia(self):
        ath = jnp.pi / 4
        aI = jnp.asarray([[2., 3., 5.]])
        aq = jnp.asarray([[0., 0., jnp.sin(ath/2), jnp.cos(ath/2)]])
        ao = jnp.asarray([[3., 7., 11.]])

        np.testing.assert_allclose((core.rotate_inertia(aI, aq)[0]) @
                                   (util.rotate_vec_by_quat(ao, aq)[0]),
                                   util.rotate_vec_by_quat(jnp.atleast_2d(aI[0] * ao[0]), aq)[0])

    def test_angular_acc(self):
        aT = jnp.asarray([1., 0., 0.])
        aI = jnp.asarray([1., 1., 1.])
        aq = jnp.asarray([1., 0., 0., 0.])
        ao = jnp.asarray([0., 0., 0.])

        np.testing.assert_allclose(core.angular_acc(aT, aI, aq, ao),
                                   jnp.asarray([[1., 0. ,0.]]))



if __name__ == "__main__":
    unittest.main()
