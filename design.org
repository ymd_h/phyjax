#+options: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+options: author:t broken-links:nil c:nil creator:nil
#+options: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+options: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+options: timestamp:t title:t toc:t todo:t |:t
#+title: design
#+date: <2022-01-30 Sun>
#+author: Hiroyuki Yamada
#+language: en
#+select_tags: export
#+exclude_tags: noexport
#+creator: Emacs 27.2 (Org mode 9.4.6)

* Rotation of Vector by Quaternion

There are other implementations which forcus on speed, however, as far
as we tested, all of them had similar speed under
~@jax.jit~. Moreover, ~qvq^{-1}~ had stably smaller calculation
errors.

#+begin_src python
@jax.jit
def rotate_perpendicular_explicitly(vec: jnp.ndarray, quat: jnp.ndarray):
    vec = jnp.atleast_2d(vec)
    quat = jnp.atleast_2d(quat)

    _cos = quat[:, 3:]
    assert _cos.shape == (quat.shape[0], 1), f"{_cos.shape}"
    _sin = jnp.sqrt(1.0-jnp.square(_cos))
    assert _sin.shape == (quat.shape[0], 1), f"{_sin.shape}"
    q_vec = jnp.where(_sin != jnp.asarray([[0., 0., 0.]]), quat[:,:3] / _sin, jnp.asarray([[1., 0., 0.]]))
    assert q_vec.shape == vec.shape, f"{q_vec.shape}"

    vec_para = (vec * q_vec).sum(axis=1, keepdims=True) * q_vec
    vec_perp = vec - vec_para

    _sin2 = 2*_sin*_cos
    _cos2 = _cos*_cos - _sin*_sin

    return vec_para + _cos2 * vec_perp + _sin2 * jnp.cross(q_vec, vec_perp)

@jax.jit
def optimized_number_of_calculation(vec: jnp.ndarray, quat: jnp.ndarray):
    vec = jnp.atleast_2d(vec)
    quat = jnp.atleast_2d(quat)

    q_vec = quat[:, :3]
    q_w = quat[:, 3:]

    return vec + 2*jnp.cross(q_vec, jnp.cross(q_vec, vec) + q_w*vec)
#+end_src


* Angular Velocity vs Angular Momentum

Angular velocity is more understandable for human, however,
integrating its differential equation easily diverges. On the other
hand, differential equation of angular momentum explicitly conserves
angular momentum and torque, so that simulation results are generally
more stable.
