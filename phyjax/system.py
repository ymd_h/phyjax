from typing import Callable, Tuple, Iterable

import jax
import jax.numpy as jnp

from .util import take
from .core import State, angular_acc
from .integrator import RungeKutta4

class System:
    def __init__(self, m, I, g=(0.,0.,0.), dt: float = 0.01,
                 contacts: Iterable = tuple(),
                 constraints: Iterable = tuple(),
                 integrator: Callable = RungeKutta4):
        self.m = jnp.asarray(m).reshape((-1,1))
        self.I = jnp.asarray(I).reshape((-1,3))

        if self.m.shape[0] != self.I.shape[0]:
            raise ValueError("m (mass) and I (inertia) must have same length." +
                             f" m: {self.m.shape}, I: {self.I.shape}")

        self.g = jnp.asarray(g)
        self.dt = dt
        self.contacts = contacts
        self.constraints = constraints
        self.integrator = integrator

    def a(self, s: State, act: jnp.ndarray, extF: Iterable[Callable] = tuple()):
        f = jnp.zeros_like(s.v)
        T = jnp.zeros_like(s.omega)

        for extf in extF:
            _f, _T = extf(s)
            f = f + _f
            T = T + _T

        acc = f / self.m + self.g

        for c in self.constraints:
            acc, _T = c.compensate(s, acc)
            T = T + _T * self.m

        return acc, T

    def step(self, s: State, act: jnp.ndarray,
             extF: Iterable[Callable[[State], Tuple[jnp.ndarray, jnp.ndarray]]] = tuple()):
        """
        Step forward physics system

        Parameters
        ----------
        s : State
            Current state
        act : jnp.ndarray
            Action
        extF : Iterable[Callable[[State], jnp.ndarray, jnp.ndarray]]
            Set of external force/torque. These Callables take ``State`` and
            return force (``jnp.ndarray``) and torque (``jnp.ndarray``).
        """
        touch = [(c.touch_idx(s), c) for c in self.contacts]
        touch = [tt for tt in touch if tt[0].shape[0] > 0]

        def acc(s: State):
            a, T = self.a(s, act, extF)
            for t, c in touch:
                _a, _T = c.touch_compensation(take(a, t))
                a = a.at[t].set(_a)
                T = T.at[t].add(_T)

            return a, T

        s = self.integrator(s, acc, I, self.dt)

        for c in self.contacts:
            s = c.collisional_compensation(s)

        return s
