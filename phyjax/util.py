import jax
import jax.numpy as jnp

def take(a: jnp.ndarray, idx: jnp.ndarray):
    return a.take(idx, axis=0).reshape(idx.shape[0], *(a.shape[1:]))


@jax.jit
@jax.vmap
def matmul(a: jnp.ndarray, b: jnp.ndarray):
    return a @ b

@jax.jit
def normalize(quat: jnp.ndarray):
    quat = jnp.atleast_2d(quat)
    size = jnp.linalg.norm(quat, axis=1, keepdims=True)
    return quat.at[:].divide(size)

@jax.jit
@jax.vmap
def rot2quat(rot: jnp.ndarray):
    theta = jnp.linalg.norm(rot)
    half_theta = theta / 2
    cos = jnp.cos(half_theta)
    sin = jnp.sin(half_theta)

    q = jnp.zeros(shape=(4,))
    q = q.at[:3].set((rot / theta) * sin)
    q = q.at[3].set(cos)
    return q

@jax.jit
@jax.vmap
def quat_mul(quat1: jnp.ndarray, quat2: jnp.ndarray):
    v1 = quat1[:3]
    w1 = quat1.at[3].get(mode="fill", fill_value=0)
    v2 = quat2[:3]
    w2 = quat2.at[3].get(mode="fill", fill_value=0)

    return jnp.concatenate((jnp.cross(v1,v2) + w2*v1 + w1*v2,
                            jnp.atleast_1d(w1*w2 - jnp.inner(v1,v2))),
                           axis=0)


@jax.jit
@jax.vmap
def inv_quat(q: jnp.ndarray):
    """
    Calculate inverse quaternion (inverse rotation)

    Parameters
    ----------
    q : jnp.ndarray
        Rotational quaternion. The shape is ``(N, 4)``.

    Returns
    -------
    : jnp. ndarray
        Inverted quaternion
    """
    qbar = jnp.array(q)
    qbar = qbar.at[:3].set(-q[:3])
    return qbar

@jax.jit
def rotate_vec_by_quat(vec: jnp.ndarray, quat: jnp.ndarray):
    """
    Rotate 3-vector by quaternion.

    vec : jnp.ndarray
        Vector to be ratated. The shape is ``(N, 3)``.
    quat : jnp.ndarray
        Rotational quaternion. The shape is ``(N, 4)``.

    Returns
    -------
    : jnp.ndarray
        Rotated vector
    """
    vec = jnp.atleast_2d(vec)
    quat = jnp.atleast_2d(quat)

    qbar = inv_quat(quat)
    return quat_mul(quat, quat_mul(vec, qbar))[:,:3]
