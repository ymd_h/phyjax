from typing import Callable

import jax
import jax.numpy as jnp

from .util import take
from .core import State

class Constraint:
    def __init__(self, idx,  G: Callable[[jnp.ndarray], float]):
        self.idx = jnp.asarray(idx, dtype="int32")

        dG = jax.vmap(jax.grad(G))
        hessian = jax.vmap(jax.jacfwd(jax.jacrev(G)))

        @jax.jit
        def a_compensation(s: State, a: jnp.ndarray):
            x = take(s.x, self.idx)
            v = take(s.v, self.idx)
            dg = dG(x)

            dga = (dg * take(a, self.idx)).sum(axis=1, keepdims=True)

            A = jnp.tensordot(jnp.tensordot(hessian(x), v, axes=(2,1)).sum(axis=2), v, axes=(1, 1)) + dga

            dg = dg / jnp.square(dg).sum(axis=1, keepdims=True)

            return -A*dg

        self.a_compensation = a_compensation


class FixedRadiusConstraint(Constraint):
    def __init__(self, idx, radius: float,
                 pos = (0, 0, 0), *,
                 dtype="float32"):
        pos = jnp.asarray(pos, dtype=dtype)

        def G(x: jnp.ndarray):
            return jnp.square(x - pos).sum() - radius*radius

        super().__init__(idx, G)

    def compensate(self, s: State, a: jnp.ndarray):
        da = self.a_compensation(s, a)

        a = a.at[self.idx].add(da)
        return a, jnp.zeros_like(a)
