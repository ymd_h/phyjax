import jax
import jax.numpy as jnp

from .util import matmul, inv_quat, rotate_vec_by_quat, normalize

class State:
    def __init__(self, x: jnp.ndarray, v: jnp.ndarray,
                 quat: jnp.ndarray, L: jnp.ndarray,
                 dtype="float32"):
        self.x = jnp.asarray(x, dtype=dtype)
        self.v = jnp.asarray(v, dtype=dtype)
        self.quat = normalize(jnp.asarray(quat, dtype=dtype))
        self.L = jnp.asarray(L, dtype=dtype)
        self.dtype = dtype


def flatten_State(s:State):
    return [s.x, s.v, s.quat, s.L], s.dtype

def unflatten_State(a, d):
    return State(x=d[0], v=d[1], quat=d[2], L=d[3], dtype=a)

jax.tree_util.register_pytree_node(State, flatten_State, unflatten_State)


@jax.jit
@jax.vmap
def rotate_inertia(I: jnp.ndarray, quat: jnp.ndarray):
    """
    Rotate diagonal inertia

    Parameters
    ----------
    I : jnp.ndarray
        Diagonal inertia to be ratated
    quat : jnp.ndarray
        Rotational quaternion

    Returns
    -------
    Ig : jnp.ndarray
        Rotated inertia
    """
    quat = jnp.broadcast_to(jnp.atleast_2d(quat), (3,4))
    qbar = inv_quat(quat)

    I = jnp.broadcast_to(jnp.diag(I), (3,3,3))
    e = rotate_vec_by_quat(jnp.eye(3), qbar)
    return jnp.transpose(rotate_vec_by_quat(matmul(I, e), quat))


@jax.jit
def angular_velocity(I: jnp.ndarray, quat: jnp.ndarray, L: jnp.ndarray):
    """
    Calculate Angular Velocity from Angular Momentum

    Parameters
    ----------
    I : jnp.ndarray
        Diagonal inertia
    quat : jnp.ndarray
        Quaternion
    L : jnp.ndarray
        Angular momentum

    Returns
    -------
    jnp.ndarray
        Angular velocity
    """
    I = jnp.atleast_2d(I)
    quat = jnp.atleast_2d(quat)
    L = jnp.atleast_2d(L)

    return matmul(rotate_inertia(1./I, quat), L)


@jax.vmap
def _ang_acc(T: jnp.ndarray, Ig: jnp.ndarray, inv_Ig: jnp.ndarray,
             omega: jnp.ndarray):
    return inv_Ig @ (T - jnp.cross(omega, Ig @ omega))


@jax.jit
def angular_acc(T: jnp.ndarray, I: jnp.ndarray,
                quat: jnp.ndarray, omega: jnp.ndarray):
    """
    Calculate Angular Acceleration

    Parameters
    ----------
    T : jnp.ndarray
        Torque. (N, 3)
    I : jnp.ndarray
        Diagonal Inertia. (N, 3)
    quat : jnp.ndarray
        Quaternion of rotation. (N, 4)
    omega : jnp.ndarray
        Angular velocity vector. (N, 3)

    Returns
    -------
    alpha : jnp.ndarray
        Angular acceleration. (N, 3)
    """
    T = jnp.atleast_2d(T)
    I = jnp.atleast_2d(I)
    quat = jnp.atleast_2d(quat)
    omega = jnp.atleast_2d(omega)

    Ig = rotate_inertia(I, quat)
    inv_Ig = jnp.linalg.inv(Ig)

    return _ang_acc(T, Ig, inv_Ig, omega)


@jax.jit
@jax.vmap
def _dquat_dt(q: jnp.ndarray, rot: jnp.ndarray):
    return 0.5 * jnp.asarray((+rot[2]*q[1]-rot[1]*q[2]+rot[0]*q[3],
                              -rot[2]*q[0]+rot[0]*q[2]+rot[1]*q[3],
                              +rot[1]*q[0]-rot[0]*q[1]+rot[2]*q[3],
                              -rot[0]*q[0]-rot[1]*q[1]-rot[2]*q[2]))

@jax.jit
def dquat_dt(quat: jnp.ndarray, L: jnp.ndarray, I: jnp.ndarray):
    """
    Calculate d(quat)/dt

    Parameters
    ----------
    quat : jnp.ndarray
        Current position-orientation quaternion
    L : jnp.ndarray
        Angular Momentum
    I : jnp.ndarray
        Inertia

    Returns
    -------
    : jnp.ndarray
        d(quat)/dt
    """
    rot = angular_velocity(I, q, L)
    return _dquat_dt(quat, rot)
