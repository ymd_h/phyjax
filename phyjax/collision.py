import jax
import jax.numpy as jnp

from .util import take
from .core import State


def between(x, a, b):
    return (a <= x) & (x <= b)


class SphereGroundContact:
    def __init__(self, idx, radius: float,
                 pos = (0, 0, 0), norm = (0, 0, 1), e=1.0, *,
                 dtype="float32"):
        self.idx = jnp.asarray(idx, dtype="int32")
        self.pos = jnp.asarray(pos, dtype=dtype)
        self.norm = jnp.asarray(norm, dtype=dtype)
        self.norm = self.norm / jnp.linalg.norm(self.norm)
        self.radius = radius
        self.e = e

    def norm_x(self, x: jnp.ndarray):
        return ((x - self.pos) * (self.norm)).sum(axis=1) - self.radius

    def norm_v(self, v: jnp.ndarray):
        return (v * self.norm).sum(axis=1)

    def touch_compensation(self, a: jnp.ndarray):
        return a * (1 - self.norm), jnp.zeros_like(a)

    def touch_idx(self, s: State):
        return self.idx[(between(self.norm_x(take(s.x, self.idx)), 0, 1e-3) &
                         between(self.norm_v(take(s.v, self.idx)), -1e-3, 0))]

    def collisional_compensation(self, s: State):
        v = s.v
        hit_idx = self.idx[self.norm_x(take(s.x, self.idx)) < 0]

        v = v.at[hit_idx].add(- take(v, hit_idx) * self.norm * (1 + self.e))
        return State(x=s.x, v=v, quat=s.quat, omega=s.omega, dtype=s.dtype)
