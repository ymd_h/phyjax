from typing import Callable

import jax
import jax.numpy as jnp

from .core import State, dquat_dt

def SymplecticEuler(s: State, a: Callable[[State], jnp.ndarray],
                    I: jnp.ndarray, dt):
    a, T = a(s)

    v_next = s.v + dt * a
    x_next = s.x + dt * v_next

    L_next = s.L + dt * T
    q_next = s.quat  + dt * dquat_dt(s.quat, L_next, I)
    return State(x=x_next, v=v_next, quat=q_next, L=L_next, dtype=s.dtype)


def Verlet(s: State, a: Callable[[State], jnp.ndarray],
           I: jnp.ndarray, dt):
    x_half = s.x + 0.5 * dt * s.v
    q_half = s.quat + 0.5 * dt * dquat_dt(s.quat, s.L, I)

    a, T = a(State(x=x_half, v=s.v, quat=q_half, L=s.L, dtype=s.dtype))
    v_next = s.v + dt * a
    L_next = s.omega + dt * T

    x_next = x_half + 0.5 * dt * v_next
    q_next = q_half + 0.5 * dt * dquat_dt(q_half, L_next, I)
    return State(x=x_next, v=v_next, quat=q_next, L=L_next, dtype=s.dtype)


h1 = 1.0 / (2.0 - jnp.cbrt(2.0))
h0 = 1.0 - 2*h1
def Sympletic4(s: State, a: Callable[[State], jnp.ndarray],
               I: jnp.ndarray, dt):
    return Verlet(Verlet(Verlet(s, a, I, h1*dt), a, I, h0*dt),a, I, h1*dt)


def RungeKutta4(s: State, a: Callable[[State], jnp.ndarray],
                I: jnp.ndarray, dt):
    """
    dx/dt = v
    dv/dt = a(s)

    dq/dt = 0.5 q omega
    d(omega)/dt = I^(-1) (t - omega x I omega)
    """
    x_k1 = s.v
    v_k1, L_k1 = a(s)
    q_k1 = dquat_dt(s.quat, s.L, I)

    # s1 = s + 0.5*dt*k1
    s1 = State(x = s.x+0.5*dt*x_k1,
               v = s.v+0.5*dt*v_k1,
               quat  = s.quat  + 0.5*dt*q_k1,
               L = s.L + 0.5*dt*L_k1,
               dtype=s.dtype)

    x_k2 = s1.v
    v_k2, L_k2 = a(s1)
    q_k2 = dquat_dt(s1.quat, s1.L, I)

    # s2 = s + 0.5*dt*k2
    s2 = State(x = s.x+0.5*dt*x_k2,
               v = s.v+0.5*dt*v_k2,
               quat  = s.quat  + 0.5*dt*q_k2,
               L = s.L + 0.5*dt*L_k2,
               dtype=s.dtype)

    x_k3 = s2.v
    v_k3, L_k3 = a(s2)
    q_k3 = dquat_dt(s2.quat, s2.L, I)

    # s3 = s + dt*k3
    s3 = State(x = s.x+dt*x_k3,
               v = s.v+dt*v_k3,
               quat  = s.quat  + dt*q_k3,
               L = s.L + dt*L_k3,
               dtype=s.dtype)

    x_k4 = s3.v
    v_k4, L_k4 = a(s3)
    q_k4 = dquat_dt(s3.quat, s3.L, I)

    # s_next = s + (1/6) * (k1 + 2*k2 + 2*k3 + k4) * dt
    s_next = State(x = s.x + (dt/6) * (x_k1 + 2*x_k2 + 2*x_k3 + x_k4),
                   v = s.v + (dt/6) * (v_k1 + 2*v_k2 + 2*v_k3 + v_k4),
                   quat  = s.quat  + (dt/6) * (q_k1 + 2*q_k2 + 2*q_k3 + q_k4),
                   L = s.L + (dt/6) * (L_k1 + 2*L_k2 + 2*L_k3 + L_k4),
                   dtype = s.dtype)
    return s_next
